#!perl -T
use strict;
use warnings; 
use Test::More;

use lib ".";

plan tests => 1;

BEGIN {
    use_ok( 'Pare' ) || print "Bail out!\n";
}

diag( "Testing Pare $Pare::VERSION, Perl $], $^X" );
